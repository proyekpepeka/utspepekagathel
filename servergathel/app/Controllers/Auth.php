<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\ModelAuth;


class Auth extends BaseController
{
    use ResponseTrait;
    public function index()
    {
        $validation = \Config\Services::validation();
        $rules      = [
            'email' => [
                'rules' => 'required|valid_email',
                'errors'=>[
                    'required'=> 'silakan masukkan email',
                    'valid_email' => 'silakan masukkan email yang valid'
                ]    
            ],
            'password' => [
                'rules' => 'required',
                'errors'=>[
                    'required'=> 'silakan masukkan password'
                ]
            ]
        ];
        $validation->setRules($rules);
        if(!$validation->withRequest($this->request)->run()){
            return $this->fail($validation->getErrors());
        }

        $model = new ModelAuth();
        
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $data = $model->getEmail($email);
        if($data['password'] != md5($password)){
            return $this->fail("password tidak sesuai");
        }

        helper('jwt');
        $response = [
            'message'      => "otentikasi berhasil dilakukan",
            'data'         => $data,
            'access_token' => createJWT($email)
        ];
        return $this->respond($response);
    }
}
