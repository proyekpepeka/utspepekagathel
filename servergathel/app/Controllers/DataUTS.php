<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\ModelDataUTS;

class DataUTS extends BaseController
{
    use ResponseTrait;
    function __construct()
    {
        $this->model = new ModelDataUTS();
    }
    public function index()
    {
        // return view('welcome_message');
        $data = $this->model->orderBy('username', 'asc')->findAll();
        return $this->respond($data, 200);
    }

    public function show($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if($data){
            return $this->respond($data,200);
        }else{
            return $this->failNotFound("data tidak ditemukan untuk id $id");
        }
    }

    public function create()
    {
        // $data = [
        //     'lname'     =>$this->request->getVar('lname'),
        //     'username'  =>$this->request->getVar('username'),
        //     'email'     =>$this->request->getVar('email'),
        //     'password'  =>md5($this->request->getVar('password')) 


        //     'username'  =>$this->request->getVar('username'),
        //     'data1'     =>$this->request->getVar('data1'),
        //     'data2'     =>$this->request->getVar('data2'),

            
        // ];    
        //input data dengan getVar seperti di atas 
        //bisa diganti dengan baris di bawah ini
        $data = $this->request->getPost();

        // $this->model->save($data);
        if(!$this->model->save($data)){
            return $this->fail($this->model->errors());
        }

        $response = [
            'status' => 201,
            'error'  => null,
            'messages'=>[
                'success' => 'berhasil memasukkan data UTS'
            ]
        ];
        return $this->respond($response);
    }

    public function update($id = null)
    {
        $data = $this->request->getRawInput();
        $data['id'] = $id;

        $isAda = $this->model->where('id', $id)->findAll();
        if(!$isAda){
            return $this->failNotFound("Data tidak ditemukan untuk id $id");
        }

        if(!$this->model->save($data)){ //kalo ada eror pas save
            return $this->fail($this->model->errors()); //validasi biar ga bisa kosong
        }

        $response = [
            'status' => 200,
            'error'  => null,
            'messages'=>[
                'success' => "berhasil mengupdate data UTS dengan id $id"
            ]
            ];
        return $this->respond($response);
    }

    public function delete($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if($data)
        {
            $this->model->delete($id);
            $response = [
                'status'=>200,
                'error'=>null,
                'messages'=>[
                    'success'=>'data berhasil dihapus'
                ]
                ];
                return $this->respondDeleted($response);
        }else{
            return $this->failNotFound("data yang hendak dihapus tidak ditemukan");
        }
    }
}
