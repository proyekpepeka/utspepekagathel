<?php

namespace App\Models;

use CodeIgniter\Model;

class ModelDataUTS extends Model{
    // protected $table = "users";
    protected $table = "dataproject";
    protected $pkey  = "id";
    protected $allowedFields = ['username','data1','data2'];

    protected $validationRules = [
        'username' => 'required',
        'data1'    => 'required',
        // 'email'    => 'requires|valid_email',
        'data2'    => 'required'
    ];

    protected $validationMessages = [
        'username' => [
            'required' => 'silakan masukkan username'
        ],
        // 'email' => [
        //     'required' => 'silakan masukkan email yang sesuai',
        //     'valid_email' => 'email yang dimasukkan tidak valid'
        // ],
        'data1' => [
            'required' => 'silakan masukkan data pertama'
        ],
        'data2' => [
            'required' => 'silakan masukkan data kedua'
        ]
    ];
}