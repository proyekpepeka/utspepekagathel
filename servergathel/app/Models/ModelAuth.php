<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ModelAuth extends Model 
{
    protected $table = "users";
    protected $pkey  = "id";
    protected $allowedFields = ['lname','username','email','password'];

    function getEmail($email)
    {
        $builder = $this->table("users");
        $data = $builder->where("email", $email)->first();
        if(!$data)
        {
            throw new Exception("Data otentikasi tidak ditemukan");

        }
        return $data;
    }
}