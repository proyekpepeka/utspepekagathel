<?php
use App\Models\ModelAuth;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

function getJWT($authHeader)
{
    if (is_null($authHeader))
    {
        throw new Exception("Otentikasi JWT gagal");
    }
    return explode(" ", $authHeader)[1];
}

function validateJWT($encodedToken)
{
    $key = getenv('JWT_SECRET_KEY');
    $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
    $modelAuth    = new ModelAuth();

    $modelAuth->getEmail($decodedToken->email);
}

function createJWT($email)
{
    $requestTime = time();
    $tokenTime   = getenv('JWT_TIME_TO_LIVE');
    $expiration  = $requestTime + $tokenTime;
    $payload     = [
        'email' => $email,
        'iat'   => $requestTime,
        'exp'   => $expiration
    ];
    $jwt = JWT::encode($payload, getenv('JWT_SECRET_KEY'), 'HS256');
    return $jwt;
}